const mongoose = require('mongoose');

const poissonSchema = mongoose.Schema({
  count: { type: Number, required: false },
  api_version: { type: String, required: false },
  data: { type: String, required: false},

});

module.exports = mongoose.model('Poisson', poissonSchema);