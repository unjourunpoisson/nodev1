const http = require('http');
const app = require('./app');

//app.set('port', process.env.PORT || 3000);
const PORT = process.env.PORT || 3000;
const server = http.createServer(app);

server.listen(PORT,() =>{
    console.log(`Server is running on port ${PORT}`)
} );
