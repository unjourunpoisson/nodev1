import os

def test_api():
    staging_url = os.environ.get("HEROKU_STAGING_URL/api/public/codeStation/01800026")

    with open('test/test.json') as f:
        answer = f.read()

    with urllib.request.urlopen(staging_url) as response:
        staging_answer = response.read().decode('utf-8')

    print(staging_answer)
    print(answer)

    assert staging_answer == answer

